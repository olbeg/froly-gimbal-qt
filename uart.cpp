#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include "uart.hpp"

Uart::Uart(QObject *parent) : QObject(parent) {

}

bool Uart::isOpen() const {
    return open;
}

bool Uart::openPort(QString name, size_t baudrate) {
    serial_fd = ::open(name.toStdString().c_str(), O_RDWR | O_NOCTTY | O_NDELAY);
    if (serial_fd == -1)
        return false;

    struct termios att;
    if (tcgetattr(serial_fd, &att) < 0)
        return false;

    att.c_iflag = 0;
    att.c_oflag = 0;
    att.c_lflag = 0;
    att.c_cflag = 0;

    att.c_cflag = CS8 | CREAD | baudrate;
    att.c_cc[VMIN] = 5;
    att.c_cc[VTIME] = 2;

    if (cfsetispeed(&att, baudrate) != 0)
        return false;

    if (tcsetattr(serial_fd, TCSANOW, &att) != 0)
        return false;

    open = true;
    return open;
}

bool Uart::write(const void *data, size_t length) {
    if (!isOpen())
        return false;

    auto size = ::write(serial_fd, data, length);
    if (size != length)
        return false;

    fd_set fds;
    struct timeval tv;
    FD_ZERO(&fds);
    FD_SET(serial_fd, &fds);
    tv.tv_sec = 0;
    tv.tv_usec = 100000;

    uint8_t buffer[32] = {0};

    size = select(serial_fd + 1, &fds, NULL, NULL, &tv);
    if (size > 0) {
        size = ::read(serial_fd, buffer, sizeof(buffer));
        if (size > 0) {
            for (size_t i = 0; i < sizeof(buffer); ++i)
                if (memcmp("ACK", &buffer[i], 3) == 0)
                    return true;
        }
    }

    return false;
}
