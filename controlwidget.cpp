#include <QApplication>
#include <QDebug>
#include <QButtonGroup>

#include <termios.h>
#include "controlwidget.hpp"
#include "uart.hpp"
#include "external/common/include/protocol.h"

ControlWidget::ControlWidget(QWidget *parent) : QMainWindow(parent) {   
    QWidget *main = new QWidget(this);
    setCentralWidget(main);
    QVBoxLayout *vbox = new QVBoxLayout(main);
    vbox->setMargin(10);

    QLabel *status = new QLabel(main);
    QString portname = QApplication::arguments()[1];
    status->setText(QString("<b>Port:</b> %1, <b>Baudrate:</b> 115200").arg(portname));
    if (uart.openPort(portname, B115200)) {
        status->setText(status->text() + "<br><b>Status: <span style='color:green'>open</span></b>");
    } else {
        status->setText(status->text() + "<br><b>Status: <span style='color:red'>closed</span></b>");
    }
    vbox->addWidget(status);
    vbox->addSpacing(10);

    QHBoxLayout *hbox = new QHBoxLayout();
    btn_check_connection = new QPushButton("Set", main);
    btn_check_connection->setFixedHeight(22);

    hbox->addWidget(btn_check_connection);

    connect(btn_check_connection, SIGNAL(clicked()), this, SLOT(on_btn_check_connection()));

    edit_yaw = new QLineEdit("150", main);
    edit_pitch = new QLineEdit("150", main);
    edit_yaw->setFixedWidth(150);
    edit_pitch->setFixedWidth(150);
    QLabel *l1 = new QLabel("Yaw", main);
    QLabel *l2 = new QLabel("Pitch", main);
    l1->setFixedWidth(30);
    l2->setFixedWidth(30);

    QHBoxLayout *hbox1 = new QHBoxLayout();
    hbox1->addWidget(l1);
    hbox1->addWidget(edit_yaw);
    hbox1->addStretch();

    QHBoxLayout *hbox2 = new QHBoxLayout();
    hbox2->addWidget(l2);
    hbox2->addWidget(edit_pitch);
    hbox2->addStretch();

    vbox->addLayout(hbox1);

    slider_yaw = new QSlider(Qt::Horizontal, main);
    slider_yaw->setMaximum(210);
    slider_yaw->setMinimum(90);
    slider_yaw->setValue(150);
    vbox->addWidget(slider_yaw);
    connect(slider_yaw, SIGNAL(valueChanged(int)), this, SLOT(on_slider_changed(int)));

    vbox->addLayout(hbox2);

    slider_pitch = new QSlider(Qt::Horizontal, main);
    slider_pitch->setMaximum(210);
    slider_pitch->setMinimum(90);
    slider_pitch->setValue(150);
    vbox->addWidget(slider_pitch);
    connect(slider_pitch, SIGNAL(valueChanged(int)), this, SLOT(on_slider_changed(int)));

    vbox->addLayout(hbox);
    vbox->addSpacing(10);

    edit_dac = new QLineEdit("4000", main);
    vbox->addWidget(edit_dac);

    btn_start = new QPushButton("Enable PWM", main);
    btn_start->setFixedHeight(22);
    vbox->addWidget(btn_start);
    connect(btn_start, SIGNAL(clicked()), this, SLOT(on_btn_start()));

    btn_stop = new QPushButton("Disable PWM", main);
    btn_stop->setFixedHeight(22);
    vbox->addWidget(btn_stop);
    connect(btn_stop, SIGNAL(clicked()), this, SLOT(on_btn_stop()));

    QButtonGroup *group = new QButtonGroup(this);
    camera1 = new QRadioButton("camera 1", main);
    camera2 = new QRadioButton("camera 2", main);
    group->addButton(camera1);
    group->addButton(camera2);
    connect(camera1, SIGNAL(clicked()), this, SLOT(on_btn_check_connection()));
    connect(camera2, SIGNAL(clicked()), this, SLOT(on_btn_check_connection()));
    camera1->setChecked(true);

    vbox->addSpacing(10);
    vbox->addWidget(camera1);
    vbox->addWidget(camera2);
    vbox->addSpacing(20);
    vbox->addStretch();
    text = new QLabel(main);
    vbox->addWidget(text);

    setFixedSize(265, 385);

    connect(&refresh, SIGNAL(timeout()), this, SLOT(on_refresh()));
    refresh.start(1000);
}

ControlWidget::~ControlWidget() {

}

void ControlWidget::on_refresh() {
    setText("- - -");
}

void ControlWidget::on_slider_changed(int value) {
    if (QObject::sender() == slider_pitch) {
        edit_pitch->setText(QString::number(value));
        on_btn_check_connection();
    }
    if (QObject::sender() == slider_yaw) {
        edit_yaw->setText(QString::number(value));
        on_btn_check_connection();
    }
}

void ControlWidget::on_btn_check_connection() {
    if (uart.isOpen()) {
        ctrl_message message = { PACKET_HEADER_INIT(CTRL_MSG) };

        bool valid;
        auto yaw = edit_yaw->text().toInt(&valid);
        if (valid) {
            message.data.ctrl.yaw = yaw;
            slider_yaw->setValue(yaw);
        } else {
            qDebug() << "invalid yaw value";
            setText("invalid yaw value");
            return;
        }
        auto pitch = edit_pitch->text().toInt(&valid);
        if (valid) {
            message.data.ctrl.pitch = pitch;
            slider_pitch->setValue(pitch);
        } else {
            qDebug() << "invalid pitch value";
            setText("invalid pitch value");
            return;
        }

        if (camera1->isChecked()) {
            message.data.ctrl.camera = 1;
        } else {
            message.data.ctrl.camera = 2;
        }

        auto dac = edit_dac->text().toInt(&valid);
        if (valid) {
            message.data.ctrl.dac_data = dac;
        } else {
            qDebug() << "invalid dac value";
            setText("invalid dac value");
            return;
        }

        message.crc = PACKET_GIMBAL_CRC((uint8_t*)&message);

        if (uart.write((void*)&message.head1, sizeof(ctrl_message))) {
            qDebug() << "ACK";
            setText("ack");
        } else {
            qDebug() << "FAIL";
            setText("fail");
        }
    } else {
        setText("not connected");
    }
}

void ControlWidget::set_pwm_enable(bool value) {
    if (uart.isOpen()) {
        ctrl_message message = { PACKET_HEADER_INIT(SETTINGS_MSG) };
        message.data.settings.pwm_enable = value;
        if (uart.write((void*)&message.head1, sizeof(ctrl_message))) {
            qDebug() << "ACK";
            setText("ack");
        } else {
            qDebug() << "FAIL";
            setText("fail");
        }
    } else {
        setText("not connected");
    }
}

void ControlWidget::setText(QString text) {
    this->text->setText(QString("<b>") + text + "</b>");
}

void ControlWidget::on_btn_start() {
    set_pwm_enable(true);
}

void ControlWidget::on_btn_stop() {
    set_pwm_enable(false);
}
