#-------------------------------------------------
#
# Project created by QtCreator 2016-07-30T13:25:37
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QMAKE_CXXFLAGS = -std=c++14

TARGET = froly-gimbal-qt
TEMPLATE = app


SOURCES += main.cpp\
        controlwidget.cpp \
    uart.cpp

HEADERS  += controlwidget.hpp \
    uart.hpp \
    external/common/include/protocol.h
