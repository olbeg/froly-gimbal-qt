#pragma once

#include <QMainWindow>
#include <QLayout>
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>
#include <QSlider>
#include <QRadioButton>
#include <QTimer>

#include <uart.hpp>

class ControlWidget : public QMainWindow {
    Q_OBJECT

public:
    ControlWidget(QWidget *parent = 0);
    ~ControlWidget();

private:
    QPushButton *btn_check_connection;
    QPushButton *btn_stop, *btn_start;
    QLineEdit *edit_yaw;
    QLineEdit *edit_pitch;
    QLineEdit *edit_dac;
    QSlider *slider_yaw;
    QSlider *slider_pitch;
    QRadioButton *camera1, *camera2;
    void set_pwm_enable(bool value);
    Uart uart;
    QLabel *text;
    QTimer refresh;
    void setText(QString text);

private slots:
    void on_btn_check_connection();
    void on_btn_stop();
    void on_btn_start();
    void on_slider_changed(int value);
    void on_refresh();
};
