#include <QApplication>
#include <QDebug>
#include "controlwidget.hpp"

int main(int argc, char *argv[]) {
    if (argc < 2) {
        qDebug() << argv[0] << "<port>";
        exit(0);
    }
    QApplication a(argc, argv);
    ControlWidget w;
    w.show();

    return a.exec();
}
