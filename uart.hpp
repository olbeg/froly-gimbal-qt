#pragma once

#include <stdint.h>
#include <QVector>
#include <QObject>

class Uart : public QObject {
    Q_OBJECT
public:
    explicit Uart(QObject *parent = 0);
    bool isOpen() const;
    bool openPort(QString name, size_t baudrate);
    bool write(const void *data, size_t length);
private:
    int serial_fd = -1;
    bool open = false;

signals:

public slots:

};
